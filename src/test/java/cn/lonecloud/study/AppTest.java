package cn.lonecloud.study;

import cn.lonecloud.study.format.OrderNumThread;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue() throws InterruptedException, ExecutionException {
        long l = System.currentTimeMillis();
        CountDownLatch countDownLatch=new CountDownLatch(1000);
        List<String> stringList=new ArrayList<>(1000);
        ExecutorService executorService = Executors.newFixedThreadPool(100);
//        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 1000; j++) {
                Future<String> stringFuture = executorService.submit(new OrderNumThread());
//                if (stringFuture.isDone()){
                    stringList.add(stringFuture.get());
                    countDownLatch.countDown();
//                }
            }
//        }

        countDownLatch.await();
        System.out.println("生成订单耗时"+(System.currentTimeMillis()-l));
        System.out.println(stringList);
    }
}
