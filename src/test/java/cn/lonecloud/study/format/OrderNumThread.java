package cn.lonecloud.study.format;

import cn.lonecloud.study.ZkReentrantLock;
import cn.lonecloud.study.config.ZkConfig;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Callable;
import java.util.concurrent.locks.Lock;

/**
 * @author lonecloud
 * @version v1.0
 * @Package cn.lonecloud.study.format
 * @Description: TODO
 * @date 2018/5/23下午10:26
 */
public class OrderNumThread implements Callable<String> {

    private static int count;

    private Lock lock;

    {
        try {
            ZkConfig zkConfig=new ZkConfig();
            zkConfig.setConnectString("192.168.21.4:2181");
            zkConfig.setSessionTimeout(500000);
            lock = new ZkReentrantLock(zkConfig,"/zkLock/"+count);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public String call() throws Exception {
        lock.lock();
        SimpleDateFormat formatter=new SimpleDateFormat("YYYYMMddHHmmss");
        String format = formatter.format(new Date());
        String s = format + count++;
        lock.unlock();
        return s;
    }
}
