package cn.lonecloud.study.config;

/**
 * @author lonecloud
 * @version v1.0
 * @Package cn.lonecloud.study.config
 * @Description: TODO
 * @date 2018/5/23下午6:07
 */
public class ZkConfig {


    private String connectString;

    private int sessionTimeout;

    public String getConnectString() {
        return connectString;
    }

    public void setConnectString(String connectString) {
        this.connectString = connectString;
    }

    public int getSessionTimeout() {
        return sessionTimeout;
    }

    public void setSessionTimeout(int sessionTimeout) {
        this.sessionTimeout = sessionTimeout;
    }
}
